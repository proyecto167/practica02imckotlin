package com.example.practica02imckotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import java.text.DecimalFormat

class MainActivity : AppCompatActivity() {
    private lateinit var txtAltura2: EditText
    private lateinit var txtPeso2: EditText
    private lateinit var lblIMC2: TextView
    private lateinit var btnCalcular: Button
    private lateinit var btnLimpiar: Button
    private lateinit var btnRegresar: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Relacionar los objetos
        txtAltura2 = findViewById(R.id.txtAltura)
        txtPeso2 = findViewById(R.id.txtPeso)
        lblIMC2 = findViewById(R.id.lblIMC)
        btnCalcular = findViewById(R.id.btnCalculo)
        btnLimpiar = findViewById(R.id.btnLimpio)
        btnRegresar = findViewById(R.id.btnRegreso)

        btnCalcular.setOnClickListener {
            // Obtenemos el texto de los EditText y eliminamos cualquier espacio en blanco inicial o final
            val textoAltura = txtAltura2.text.toString().trim()
            val textoPeso = txtPeso2.text.toString().trim()

            // Verificar que no estén vacíos los campos
            if (textoAltura.isEmpty()) {
                // El EditText txtAltura2 está vacío, se muestra un mensaje de error
                txtAltura2.error = "Este campo es requerido"
            } else if (textoPeso.isEmpty()) {
                // El EditText txtPeso2 está vacío, se muestra un mensaje de error
                txtPeso2.error = "Este campo es requerido"
            } else {
                // Obtener los valores de altura y peso
                val altura = txtAltura2.text.toString().toDouble()
                val peso = txtPeso2.text.toString().toDouble()

                // Verificar que no sean 0 los valores
                if (altura != 0.0 || peso != 0.0) {
                    // Calcular IMC: el peso en kilogramos dividido por la estatura en metros al cuadrado (convertimos de cm a m la altura)
                    val alturaMetros = altura / 100
                    val imc = peso / (alturaMetros * alturaMetros)

                    // Crear un objeto DecimalFormat con el patrón de dos decimales
                    val df = DecimalFormat("#.##")
                    val imcRedondeado = df.format(imc)

                    // Mostrar el resultado en el TextView
                    lblIMC2.text = "Su IMC es: $imcRedondeado kg/m2"
                }
            }
        }

        btnLimpiar.setOnClickListener {
            txtAltura2.setText("")
            txtPeso2.setText("")
            lblIMC2.text = "Su IMC es:"
        }

        btnRegresar.setOnClickListener {
            finish()
        }

    }
}